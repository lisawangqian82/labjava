package lec01.glab;

/**
 * Created by lisa on 10/5/15.
 */
public class MoreCasting {

    public static void main(String[] args) {
        // Float.parseFloat("0.22111")
        //double dValue = (float) 0.6546546173737478493735454546364554552515252533654545453;
        //System.out.println(dValue);
        //int nC=0;
        //while (nC<10){
        //    System.out.println(++nC);

        //int nMe = 80;
        String str = "Hello";
        String str1 = "Goodbye";   //equal str1 = "Goodbye"
        System.out.println(str + str1);
        String str2 = str1;     // String is not mutable;
        str1 = str + "asdfasfd";
        System.out.println(str2);


    }
}

